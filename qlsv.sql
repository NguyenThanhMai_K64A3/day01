-- 5.1
-- create database
CREATE DATABASE QLSV;

-- create table DMKHOA
CREATE TABLE DMKHOA (
    MaKH varchar(6),
    TenKhoa varchar(30),
);

-- create table DMKHOA
CREATE TABLE SINHVIEN (
    MaSV varchar(6),
    HoSV varchar(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh datetime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int,
    PRIMARY KEY (MaSV),
);

